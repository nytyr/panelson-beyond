// ==UserScript==
// @name        Panelson Beyond
// @version     0.6.1
// @downloadURL https://bitbucket.org/zodier/panelson-beyond/raw/master/panelsonBeyond.js
// @updateURL   https://bitbucket.org/zodier/panelson-beyond/raw/master/panelsonBeyond.js
// @description Añade funcionalidades adicionales sobre el panel original Panelson
// @match       https://panelson.paynopain.com/
// @match       https://panelson.paynopain.com/Dashboard
// ==/UserScript==

// Dependencias
$.getScript("https://download-data-uri.googlecode.com/svn/trunk/js/download-data-uri.js");
$('head').append('<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" type="text/css" />');

// Config
var horasQueHacerPorDia = {
    1: 8.5, // LUNES
    2: 8.5, // MARTES
    3: 8.5, // MIERCOLES
    4: 8.5, // JUEVES
    5: 7,   // VIERNES
    6: 0,   // SABADO
    0: 0,   // DOMINGO
};

// Vars
var fechaDeHoy = new Date();
var contenidoDeLaPagina = $(".page-content");
var horasAcumuladasHoy = dameHorasQueLlevoHoy();
var overrideableStorage = new function () {
    var extension = ".overrided";

    function overridedName(itemName) {
        return itemName + extension;
    }

    return {
        getItem: function (item) {
            var overridedValue = localStorage.getItem(overridedName(item));

            if (overridedValue) {
                try {
                    var jsonValue = JSON.parse(overridedValue);
                    if (jsonValue.hasOwnProperty('value')) {
                        return jsonValue.value;
                    }
                    return jsonValue;
                } catch (ignored) {
                }

                return overridedValue;
            }

            return localStorage.getItem(item);
        },

        getItemComplete: function (item) {
            var overridedValue = localStorage.getItem(overridedName(item));

            if (overridedValue)
                return overridedValue;

            return localStorage.getItem(item);
        },

        setItem: function (item, value) {
            if (typeof value == "object") {
                value = JSON.stringify(value);
            }

            localStorage.setItem(overridedName(item), value);
        }
    }
};

// ------------------------------------------------------------- //

if (typeof(Storage) != "undefined"){
    inicializaDatos();
    guardarHistorico();
    mostrarHorasRestantesHoy();
    mostrarEstadoDeLaSemana();
    mostrarEstadoGlobal();
    mostrarMenuOpciones();

    // Opcionales
    if (localStorage.getItem('reemplazarGraficas') == "true")
        reemplazaGraficas();
    if (localStorage.getItem('limpiarCopyright') == "true")
        limpiaCopyright();
}else{
    $("html").html("Tu explorador no soporta Web Storage. ¡Actualizate, tio!");
}


$(document).ready(function () {
    setInterval(function () {
        location.reload();
    }, localStorage.getItem('refresco'));
});

// ------------------------------------------------------------- //

// Panel de opciones
$("#alertaCheckbox").change(function() {
    if (this.checked)   localStorage.setItem('alerta', true);
    else                localStorage.setItem('alerta', false);
});
$("#horaSalidaCheckbox").change(function() {
    if (this.checked)   localStorage.setItem('mostrarHoraSalida', true);
    else                localStorage.setItem('mostrarHoraSalida', false);
    location.reload();
});
$("#reemplazarGraficasCheckbox").change(function() {
    if (this.checked)   localStorage.setItem('reemplazarGraficas', true);
    else                localStorage.setItem('reemplazarGraficas', false);
    location.reload();
});
$("#limpiarCopyrightCheckbox").change(function() {
    if (this.checked)   localStorage.setItem('limpiarCopyright', true);
    else                localStorage.setItem('limpiarCopyright', false);
    location.reload();
});
$("#refrescoInput").change(function () {
    localStorage.setItem('refresco', this.value * 60 * 1000);
});
$("#nombreInput").change(function() {
    localStorage.setItem('nombre', this.value);
});
$("#nombreInput").change(function() {
    localStorage.setItem('nombre', this.value);
});
$("#exportarDatosCSVButton").click(function() {
    exportarHorasEnCSV();
});
$("#exportarDatosJSONButton").click(function() {
    exportarHorasEnJSON();
});
$("#fechaDatosInput").datepicker();
$("#fechaDatosInput").change(function() {
    $('#fechaDatosInput').datepicker('option', {dateFormat: 'dd/mm'});
    $("#horasDatosInput").val(overrideableStorage.getItem(this.value));
});
$("#guardarDatosInput").click(function() {
    $("#horasDatosInput").val(
        localStorage.setItem(
            $("#fechaDatosInput").val() + '.overrided',
            $("#horasDatosInput").val()
        )
    );
    location.reload();
});

// ------------------------------------------------------------- //

function horasQueDeberiaHaberHecho(desdeFecha) {
    var now = new Date(Date.now());
    var total = 0;
	for (var d = desdeFecha; d <= now; d.setDate(d.getDate() + 1)) {
        total += horasQueHacerPorDia[d.getDay()];
	}
    return total;
}

function horasQueHeHecho(desdeFecha){
    var now = new Date(Date.now());
    var total = 0;
    
	for (var d = desdeFecha; d <= now; d.setDate(d.getDate() + 1)) {       
        var day = d.getDate();
        if (day <= 9) day = "0".concat(day);
        var month = d.getMonth()+1;
        if (month <= 9) month = "0".concat(month);
        
        var register = "".concat(day).concat("/").concat(month);
        
        var hours = overrideableStorage.getItem(register);
        if (!hours) hours = 0;

        total += parseFloat(hours);
	}
    
    return total;
}

function primerDiaRegistrado(){    
    var primeraKey = Object.keys(localStorage)
    	.filter(function(elem){
            return /^\d\d\/\d\d$/.test(elem)
        })
    	.map(function(elem){
            function getDay(key){
                return parseInt(key.split("/")[0]);
            }
            function getMonth(key){
                return parseInt(key.split("/")[1])
            }
            
            return {
                key: elem,
                order: (getMonth(elem) * 100) + getDay(elem),
                day: getDay(elem),
                month: getMonth(elem)
            };
    	})
    	.reduce(function(prev, current, i, arr){
            if (!prev) return current;
            return current.order < prev.order? current : prev;
        });
    
    return new Date(new Date().getFullYear(), primeraKey.month -1, primeraKey.day);
}

function saldoDeHoras(){
    return horasQueHeHecho(primerDiaRegistrado()) - horasQueDeberiaHaberHecho(primerDiaRegistrado());
}

function dameHorasQueLlevoHoy() {
    var ultimoFichajeIterado = "";
    var horasQueLlevoHoy = parseFloat(0.0);
    contenidoDeLaPagina.find('.table tr').each(function () {
        // Iteramos la fila de la tabla
        fila = $(this).text().split(" ");

        // Desechamos las cabeceras
        if (fila.length != 3) return true;

        fechaArreglada = arreglaFormatoDeLaFecha(fila[0] + " " + fila[1]);
        fechaDelFichaje = new Date(fechaArreglada);
        modoDelFichaje = fila[2];

        // La fecha iterada es de HOY
        if (fechaDelFichaje.getFullYear() == fechaDeHoy.getFullYear() &&
            fechaDelFichaje.getMonth() == fechaDeHoy.getMonth() &&
            fechaDelFichaje.getDate() == fechaDeHoy.getDate()) {

            if (modoDelFichaje == "in") {
                // Si esta vacio sumamos con fechaDeHoy
                if (ultimoFichajeIterado.length != 0) {
                    fechaArreglada = arreglaFormatoDeLaFecha(ultimoFichajeIterado[0] + " " + ultimoFichajeIterado[1]);
                    fechaDeHoy = new Date(fechaArreglada);
                }
                // Sumamos las horas con el anterior out
                diferenciaDeHoras = parseFloat((fechaDeHoy - fechaDelFichaje) / 1000 / 60 / 60);
                horasQueLlevoHoy += parseFloat(diferenciaDeHoras);
            } else {
                ultimoFichajeIterado = fila;
            }

        }
    });

    return horasQueLlevoHoy.toFixed(2);
}

function dameUsuarioActual() {
    var strVal = $("body").html();
    var pattern = /monthly\.makeChart\((.*)\);/im;
    var results = pattern.exec(strVal);

    return results[1];
}

function dameHorasQueHacerPorSemana() {
    var horasQueHacerPorSemana = 0;

    for (var diaDeLaSemana in horasQueHacerPorDia) {
        horasQueHacerPorSemana += horasQueHacerPorDia[diaDeLaSemana];
    }

    return horasQueHacerPorSemana;
}

function dameHorasQueLlevoEstaSemana() {
    var fechaActual = new Date();
    var diaDeLaSemana = fechaDeHoy.getUTCDay();
    var horasQueLlevoEstaSemana = 0;

    for (var i = 1; i <= diaDeLaSemana; i++) {
        var mesActual = fechaActual.getMonth() + 1;
        if (mesActual <= 9) mesActual = '0' + mesActual;

        var diaActual = fechaActual.getDate();
        if (diaActual <= 9) diaActual = '0' + diaActual;

        var mesDiaHoy = diaActual + "/" + mesActual;
        horasRealizadas = parseFloat(overrideableStorage.getItem(mesDiaHoy));
        horasQueLlevoEstaSemana += horasRealizadas;

        fechaActual.setDate(fechaActual.getDate() - 1);
    }

    return horasQueLlevoEstaSemana;
}

function dameHorasQueDeberiaLlevarEstaSemana() {
    var diaDeLaSemanaActual = fechaDeHoy.getUTCDay();
    var horasQueDeberiaLlevarEstaSemana = 0;

    for (var diaDeLaSemana = 1; diaDeLaSemana <= diaDeLaSemanaActual; diaDeLaSemana++) {
        horasQueDeberiaLlevarEstaSemana += horasQueHacerPorDia[diaDeLaSemana];
    }

    return horasQueDeberiaLlevarEstaSemana;
}

function dameHorasRestantesHoy() {
    var diaDeLaSemana = new Date().getUTCDay();
    var horasQueHacerHoy = horasQueHacerPorDia[diaDeLaSemana];
    return (horasQueHacerHoy - horasAcumuladasHoy).toFixed(2);
}

function dameHoraDeSalidaDeHoy() {
    
    var minutosQueFaltan = dameHorasRestantesHoy() * 60;
    
    var horaActual = new Date();
    
    var newDateObj = new Date(horaActual.getTime() + minutosQueFaltan * 60000);
    
    var horaSalida = newDateObj.getHours();
    if (horaSalida <= 9) horaSalida = '0' + horaSalida ;

    var minutosSalida = newDateObj.getMinutes();
    if (minutosSalida <= 9) minutosSalida = '0' + minutosSalida;

    var horaDeSalida = horaSalida + ":" + minutosSalida;

    return horaDeSalida;
}

function inicializaDatos() {
    if (localStorage.getItem('contrato')) {
        horasQueHacerPorDia = JSON.parse(localStorage.getItem('contrato'));
    } else {
        localStorage.setItem('contrato', JSON.stringify(horasQueHacerPorDia));
    }

    if (!localStorage.getItem('nombre'))
        localStorage.setItem('nombre', "Desconocido");

    if (!localStorage.getItem('usuario'))
        localStorage.setItem('usuario', dameUsuarioActual());

    if (!localStorage.getItem('alerta'))
        localStorage.setItem('alerta', true);

    if (!localStorage.getItem('refresco'))
        localStorage.setItem('refresco', 60000);

    if (!localStorage.getItem('mostrarHoraSalida'))
        localStorage.setItem('mostrarHoraSalida', true);

    if (!localStorage.getItem('reemplazarGraficas'))
        localStorage.setItem('reemplazarGraficas', false);

    if (!localStorage.getItem('limpiarCopyright'))
        localStorage.setItem('limpiarCopyright', false);

}

function guardarHistorico() {
    guardarDatosUltimosDoceDias();
    guardarDatosHoy();
}

function guardarDatosHoy() {
    var mesActual = fechaDeHoy.getMonth() + 1;
    if (mesActual <= 9) mesActual = '0' + mesActual;

    var diaActual = fechaDeHoy.getDate();
    if (diaActual <= 9) diaActual = '0' + diaActual;

    var mesDiaHoy = diaActual + "/" + mesActual;
    localStorage.setItem(mesDiaHoy, horasAcumuladasHoy);
}

function guardarDatosUltimosDoceDias() {
    var usuario = localStorage.getItem('usuario');
    $.get("https://panelson.paynopain.com/Dashboard/getDaily/" + usuario, function (datos) {
        var datosDiarios = JSON.parse(datos);
        for (var i = 0; i < datosDiarios.msg.length; i++) {
            var fecha = datosDiarios.msg[i].Daily.created;
            var horas = datosDiarios.msg[i].Daily.total_minutes;

            localStorage.setItem(fecha, horas);
        }
    });
}

function horaEstandar(decimal) {
    var hora = String((Math.abs(decimal) / 1)).split(".")[0];
    var minutos = ((Math.abs(decimal) % 1) * 60).toFixed(0);
    if (minutos.length == 1) minutos = "0".concat(minutos);
    return hora.concat(":").concat(minutos);
}

function fechaFormatoEspanol(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
}

function arreglaFormatoDeLaFecha(fechaOriginal) {
    return fechaOriginal.replace(/\-/g, '/');
}

function mostrarEstadoDeLaSemana() {
    var horasQueLlevoEstaSemana = dameHorasQueLlevoEstaSemana();
    var horasQueDeberiaLlevarEstaSemana = dameHorasQueDeberiaLlevarEstaSemana();
    var diferenciaDeHorasEstaSemana = horasQueDeberiaLlevarEstaSemana - horasQueLlevoEstaSemana;

    var porcentajeDeHorasQueDeberiaLlevarEstaSemana = (100 / horasQueDeberiaLlevarEstaSemana * horasQueLlevoEstaSemana).toFixed(2);
    var estadoActualDeLaSemana = (diferenciaDeHorasEstaSemana > 0 ? 'Te faltan ' : 'Te sobran ') + horaEstandar(diferenciaDeHorasEstaSemana) + ' h.';
    var colorDeLaBarra = (diferenciaDeHorasEstaSemana > 0 ? 'warning' : 'success');

    if (porcentajeDeHorasQueDeberiaLlevarEstaSemana > 100)  porcentajeDeHorasQueDeberiaLlevarEstaSemana = 100;
    if (porcentajeDeHorasQueDeberiaLlevarEstaSemana < 10)   porcentajeDeHorasQueDeberiaLlevarEstaSemana = 10;

    var html = ' \
        <div class="col-md-12"> \
            <p class="help-block">Balance de horas que llevas en lo que llevamos de semana:</p> \
            <div class="progress"> \
                <div class="progress-bar progress-bar-' + colorDeLaBarra + '" style="width: ' + porcentajeDeHorasQueDeberiaLlevarEstaSemana + '%;"> \
                    <strong>Balance de esta semana:</strong>&nbsp;&nbsp;' + estadoActualDeLaSemana + ' \
                </div> \
            </div> \
        </div> \
    ';

    contenidoDeLaPagina.children(".row:nth-child(2)").prepend(html);
}

function mostrarEstadoGlobal() {
    var miSaldoDeHoras = saldoDeHoras();
    var horasQueLlevoDesdeInicio = horasQueHeHecho(primerDiaRegistrado());
    var horasQueDeberiaLlevarDesdeInicio = horasQueDeberiaHaberHecho(primerDiaRegistrado());

    var porcentajeDeHorasGenerales = (100 / horasQueDeberiaLlevarDesdeInicio * horasQueLlevoDesdeInicio).toFixed(2);
    var estadoActualGeneral = (miSaldoDeHoras < 0 ? 'Te faltan ' : 'Te sobran ') + horaEstandar(miSaldoDeHoras) + ' h.';
    var colorDeLaBarra = (miSaldoDeHoras < 0 ? 'warning' : 'success');

    if (porcentajeDeHorasGenerales > 100)  porcentajeDeHorasGenerales = 100;
    if (porcentajeDeHorasGenerales < 10)   porcentajeDeHorasGenerales = 10;

    var html = ' \
        <div class="col-md-12"> \
            <p class="help-block">Balance total de horas desde que se tienen datos registrados:</p> \
            <div class="progress"> \
                <div class="progress-bar progress-bar-' + colorDeLaBarra + '" style="width: ' + porcentajeDeHorasGenerales + '%;"> \
                    <strong>Balance global:</strong>&nbsp;&nbsp;' + estadoActualGeneral + ' \
                </div> \
            </div> \
        </div> \
    ';

    contenidoDeLaPagina.children(".row:nth-child(2)").prepend(html);
}

function mostrarMenuOpciones() {
    var style = 'style="padding: 10px 15px; border-top: 1px solid #e2e2e2 !important; font-size: 12px;"';
    var alertaChecked = (localStorage.getItem('alerta') == 'true') ? 'checked' : '';
    var horaSalidaChecked = (localStorage.getItem('mostrarHoraSalida') == 'true') ? 'checked' : '';
    var reemplazarGraficasChecked = (localStorage.getItem('reemplazarGraficas') == 'true') ? 'checked' : '';
    var limpiarCopyrightChecked = (localStorage.getItem('limpiarCopyright') == 'true') ? 'checked' : '';

    var html = ' \
        <li style="margin-top: 50px;"></li> \
        <li ' + style + '> \
            <div class="form-group"> \
                <label>Tu nombre</label> <br /> \
                <input class="form-control input-sm"  id="nombreInput" type="text" value="' + localStorage.getItem('nombre') + '" /> \
            </div> \
        </li> \
        <li ' + style + '> \
            <div class="form-group"> \
                <label>Opciones generales</label> \
                <div class="checkbox"> \
                    <label style="padding-left: 5px;"> \
                        <input style="margin-left: 0px;" id="alertaCheckbox" type="checkbox" ' + alertaChecked + '/> Mostrar alertas \
                    </label> \
                </div> \
                <div class="checkbox"> \
                    <label style="padding-left: 5px;"> \
                        <input style="margin-left: 0px;" id="horaSalidaCheckbox" type="checkbox" ' + horaSalidaChecked + '/> Mostrar hora de salida \
                    </label> \
                </div> \
                <div class="checkbox"> \
                    <label style="padding-left: 5px;"> \
                        <input style="margin-left: 0px;" id="reemplazarGraficasCheckbox" type="checkbox" ' + reemplazarGraficasChecked + '/> Reemplazar gráficas \
                    </label> \
                </div> \
                <div class="checkbox"> \
                    <label style="padding-left: 5px;"> \
                        <input style="margin-left: 0px;" id="limpiarCopyrightCheckbox" type="checkbox" ' + limpiarCopyrightChecked + '/> Limpiar copyright \
                    </label> \
                </div> \
            </div> \
        </li> \
        <li ' + style + '> \
            <div class="form-group"> \
                <label>Intervalo de refresco</label> \
                <input class="form-control input-sm" style="width: 50px;" id="refrescoInput" type="number" value="' + localStorage.getItem('refresco') / 1000 / 60 + '" /> \
                <p class="help-block">En minutos.</p> \
            </div> \
        </li> \
        <li ' + style + '> \
            <div class="form-group"> \
                <label>Exportar datos</label> \
                <div class="btn-group"> \
                    <button type="button" class="btn btn-default" id="exportarDatosCSVButton">CSV</button> \
                    <button type="button" class="btn btn-default" id="exportarDatosJSONButton">JSON</button> \
                </div> \
            </div> \
        </li> \
        <li ' + style + '> \
            <div class="form-group"> \
                <label>Modificar datos</label> \
                <input class="form-control input-sm" style="" id="fechaDatosInput" type="text" value="" placeholder="Fecha de datos" /> \
                <input class="form-control input-sm" style="" id="horasDatosInput" type="text" value="" placeholder="Horas trabajadas" /> \
                <input class="form-control input-sm" style="" id="guardarDatosInput" type="button" value="Guardar" /> \
            </div> \
        </li> \
    ';

    $(".page-sidebar-menu").append(html);
}

function mostrarHorasRestantesHoy() {
    var colorDeLasHoras = "color:red;";
    if (dameHorasRestantesHoy() <= 0) {
        colorDeLasHoras = "color:#81F781;";

        if (localStorage.getItem('alerta') == 'true')
            alert("Ya has hecho las horas de hoy, " + localStorage.getItem('nombre') + ". ¿Que haces aún aquí? ¡¡Vete!!");
    }

    var informacionHorasAcumuladas = 'Hoy llevas ' + horaEstandar(horasAcumuladasHoy) + ' horas.';
    var informacionHorasRestantes = 'Te ' + (dameHorasRestantesHoy() > 0 ? 'quedan ' : 'sobran ') + horaEstandar(dameHorasRestantesHoy()) + ' horas.';
    
    if (localStorage.getItem('mostrarHoraSalida') == 'true')
        var informacionHorasRestantes = 'Podrás irte a las ' + dameHoraDeSalidaDeHoy() + '.';
    
    // Cabecera de la tabla de accesos recientes
    contenidoDeLaPagina.find(".col-md-3").find(".caption").first().append(
        '<br /> <span style="font-size: 14px; ' + colorDeLasHoras + '">' + informacionHorasAcumuladas + '<br />' + informacionHorasRestantes + '</span>'
    );

    // Titulo de la pagina
    document.title = informacionHorasAcumuladas;
}

function exportarHorasEnCSV() {
    var datosParaDescargar = "Fecha;Horas\n";
    var nombreDelFichero = "horasPanelsonBeyond" + fechaFormatoEspanol(fechaDeHoy) + ".csv";

    for(var key in localStorage) {
        var value = overrideableStorage.getItemComplete(key);
        if (key.charAt(2) == '/' && key.charAt(5) != '.')
            datosParaDescargar = datosParaDescargar.concat( key + ';' + value + '\n' );
    }

    downloadDataURI({
        filename: nombreDelFichero,
        data: 'data:text/csv;charset=utf-8,' + encodeURIComponent(datosParaDescargar)
    });
}

function exportarHorasEnJSON() {
    var datosParaDescargar = Array();
    var nombreDelFichero = "horasPanelsonBeyond" + fechaFormatoEspanol(fechaDeHoy) + ".json";

    for(var key in localStorage) {
        var value = overrideableStorage.getItemComplete(key);
        if (key.charAt(2) == '/' && key.charAt(5) != '.')
            datosParaDescargar.push({'Fecha': key, 'Horas': value});
    }

    datosParaDescargar = JSON.stringify(datosParaDescargar);

    downloadDataURI({
        filename: nombreDelFichero,
        data: 'data:text/json;charset=utf-8,' + encodeURIComponent(datosParaDescargar)
    });
}

function reemplazaGraficas() {
    $("#myDailyAccess").attr("id","miAccesoDiario");
    var horasParaGrafica = Array();
    var diasSemana = new Array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado');

    var fechaActual = new Date();
    for (var i = 0; i < 7; i++) {
        var mesActual = fechaActual.getMonth() + 1;
        if (mesActual <= 9) mesActual = '0' + mesActual;

        var diaActual = fechaActual.getDate();
        if (diaActual <= 9) diaActual = '0' + diaActual;

        var mesDiaHoy = diaActual + "/" + mesActual;
        horasRealizadas = parseFloat(overrideableStorage.getItem(mesDiaHoy));

	var diaSemana = fechaActual.getDay();
        var color = (horasRealizadas >= horasQueHacerPorDia[diaSemana]) ? "#41924B" : "#FF5959";
	if (diaSemana == 0 || diaSemana == 6) {
		color = "#FFD700";
	}

        horasParaGrafica.push(
            {label: mesDiaHoy + "   " +  diasSemana[fechaActual.getDay()], y: horasRealizadas, color: color}
        );

        fechaActual.setDate(fechaActual.getDate() - 1);
    }

    var chart = new CanvasJS.Chart("miAccesoDiario", {
        theme: "theme2",
        axisX:{        
            labelMaxWidth: 55
        },
        axisY:{
            suffix: "h"
        },
        title:{
            text: "Datos de los últimos 7 dias"
        },
        data: [{
            type: "column",
            dataPoints: horasParaGrafica
        }],
        toolTip:{
            content: function(e){
                return "<strong>Horas realizadas:</strong> " + horaEstandar(e.entries[0].dataPoint.y);
            }
        }
    });
    chart.render();
}

function limpiaCopyright() {
    $(".canvasjs-chart-credit").html("");
    $(".footer-inner").html("");
}
